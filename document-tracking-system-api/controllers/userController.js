const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then((result, error) => {
		if(error){return false}
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then((result, error) => {
		if(error){return false}
		if(result === null){

			let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10)
			});

			return newUser.save().then((user, error) => {

				if(error) {
					return false
				} else {
					return true
				}
			})	
		}
		else{
			return false
		}
	})
}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then((result, error) => {

		if(error){return false}

		if(result == null) {
			
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {

				return { access: auth.createAccessToken(result)}

			} else {

				return false
			}
		}

	})
}

module.exports.getProfile = (data) => {

	return User.findById(data.id).then((result, error) => {
		if(error){return false}
		result.password = "";
		return result
	})
}
