const Voucher = require("../models/Voucher");
const User = require("../models/User");


module.exports.createVoucher = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {

		if (userData.isVoucherAuthor === false) {
			return false
		} 

		else {

			let newVoucher = new Voucher({

				drsNo: reqBody.drsNo,
				payee: reqBody.payee,
				particulars: reqBody.particulars,
				amount: reqBody.amount,
				from: reqBody.from,
				to: reqBody.to,
				status: reqBody.status,
				sourceOfFunds: reqBody.sourceOfFunds,
				location: reqBody.location,
				remarks: reqBody.remarks,
				history: {
					action: reqBody.status,
					remarks: reqBody.remarks
				}
			});

			return newVoucher.save().then((voucher, error) => {

				if(error) {

					return false
				
				} else {

					return true
				}
			})
		}
	})
}


module.exports.getAllVouchers = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {

		if (userData.isVoucherAuthor === false) {
			return false
		} 

		else {
			let condition = [{}]

			if(reqBody.drsNo !== ""){condition.push({drsNo: { $regex: reqBody.drsNo} })}
			if(reqBody.payee !== ""){condition.push({payee: { $regex: reqBody.payee} })}
			if(reqBody.particulars !== ""){condition.push({particulars: reqBody.particulars})}
			if(reqBody.amount !== ""){condition.push({amount: reqBody.amount})}
			if(reqBody.from !== "" && reqBody.from !== null){condition.push({from: reqBody.from})}
			if(reqBody.to !== "" && reqBody.to !== null){condition.push({to: reqBody.to})}
			if(reqBody.status !== ""){condition.push({status: reqBody.status})}	
			if(reqBody.sourceOfFunds !== ""){condition.push({sourceOfFunds: reqBody.sourceOfFunds})}	
			if(reqBody.location !== ""){condition.push({location: reqBody.location})}	
			if(reqBody.remarks !== ""){condition.push({remarks: reqBody.remarks})}

			return Voucher.find({$and: condition
				
			}).then(result => {

				return result;
			
			})	
		}
	})
}


module.exports.updateVoucher = (reqParams, reqBody, userData) => {

	return User.findById(userData.userId).then(result => {

		if (userData.isVoucherAuthor === false) {
			return false
		} 

		else {
			let updatedVoucher = {
				drsNo: reqBody.drsNo,
				payee: reqBody.payee,
				particulars: reqBody.particulars,
				amount: reqBody.amount,
				from: reqBody.from,
				to: reqBody.to,
				status: reqBody.status,
				sourceOfFunds: reqBody.sourceOfFunds,
				remarks: reqBody.remarks
			};

			return Voucher.findByIdAndUpdate(reqParams.voucherId, updatedVoucher).then((item, error) => {

				if(error) {

					return false

				} else {

					item.history.push(
						{
							action: reqBody.status,
							remarks: reqBody.remarks
						})

						return item.save().then((item, error) =>
						{
							if (error){return false}
							else{return true}
						})


					return true
				}
			})
		}
	})
}

module.exports.archiveVoucher = (reqParams, reqBody, userData) => {

	return User.findById(userData.userId).then(result => {

		if (userData.isVoucherAuthor === false) {
			return false
		} 

		else {
			let archivedVoucher = {
				
				location: reqBody.location,
				isArchive: reqBody.isArchive
			};

			return Voucher.findByIdAndUpdate(reqParams.voucherId, archivedVoucher).then((item, error) => {

				if(error) {

					return false

				} else {

					return true
				}
			})
		}
	})
}



module.exports.getVoucher = (reqParams, userData) => {


	return User.findById(userData.userId).then(result => {

		if (userData.isVoucherAuthor === false) {
			return false
		} 

		else {
			return Voucher.findById(reqParams.voucherId).then(result => {
				return result
			})
		}
	})	
}