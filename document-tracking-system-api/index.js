const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
mongoose.set('strictQuery', true);

const userRoutes = require("./routes/userRoutes");
const voucherRoutes = require("./routes/voucherRoutes");

const port = process.env.PORT || 4000;

const app = express();

mongoose.connect("", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

app.use(cors());
app.use(express.json());

app.use("/users", userRoutes);

app.use("/vouchers", voucherRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});

