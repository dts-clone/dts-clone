const mongoose = require("mongoose");

const voucherSchema = new mongoose.Schema({
	drsNo: {
		type: String,
		required: [true, "DRS Number is required"]
	},
	payee: {
		type: String,
		required: [true, "Name of payee is required"]
	},
	particulars: {
		type: String,
		required: [true, "Particular is required"]
	},
	amount: {
		type: Number, 
		required: [true, "Amount is required"]
	},
	from: {
		type: String, 
		required: [true, "Date is required"]
	},
	to: {
		type: String, 
		required: [true, "Date is required"]
	},
	status: {
		type: String,
		required: [true, "Status is required"]
	},
	sourceOfFunds: {
		type: String,
		required: [true, "Source of funds is required"]
	},
	location: {
		type: String
	},
	remarks: {
		type: String
	},
	isArchive: {
		type: Boolean,
		default: false
	},
	history : [{	
		action : {
			type : String
		},
		remarks : {
			type : String
		},
		date : {
			type : Date,
			default : new Date()
		}
	}]
});


module.exports = mongoose.model("Voucher", voucherSchema);

