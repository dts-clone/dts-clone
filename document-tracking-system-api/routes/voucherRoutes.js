const express = require("express");
const router = express.Router();
const voucherController = require("../controllers/voucherController");
const auth = require('../auth.js');

router.post("/create", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	voucherController.createVoucher(req.body, {userId: userData.id, isVoucherAuthor:userData.isVoucherAuthor}).then(resultFromController => res.send(resultFromController));
	
});

router.post("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	voucherController.getAllVouchers(req.body, {userId: userData.id, isVoucherAuthor:userData.isVoucherAuthor}).then(resultFromController => res.send(resultFromController))

});

router.put("/:voucherId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	voucherController.updateVoucher(req.params, req.body, {userId: userData.id, isVoucherAuthor:userData.isVoucherAuthor}).then(resultFromController => res.send(resultFromController));
});

router.put("/archive/:voucherId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	voucherController.archiveVoucher(req.params, req.body, {userId: userData.id, isVoucherAuthor:userData.isVoucherAuthor}).then(resultFromController => res.send(resultFromController));
});


router.get("/:voucherId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	voucherController.getVoucher(req.params, {userId: userData.id, isVoucherAuthor:userData.isVoucherAuthor}).then(resultFromController => res.send(resultFromController));
});



module.exports = router;


// router.get("/condition", (req, res) => {

// 	recordController.getByCondition().then(resultFromController => res.send(resultFromController));
// });



// router.put('/archive/:productId', auth.verify, (req, res) => {

// 	const data = {
// 		productId : req.params.productId,
// 		payload : auth.decode(req.headers.authorization).isVoucherAuthor
// 	}

// 	productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController))
// });

