// Long method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

// Short method
import { useContext } from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	//A context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Link className="navbar-brand" to="/">
		    	<div className="d-flex">
		    		<div>
			    		<img src="https://nec.up.edu.ph/wp-content/uploads/2022/11/cropped-53048065_2177810102275115_5513804758915219456_n__1_-removebg-preview-01.png" className="logo">
			    		</img>
		    		</div>
		    		<div>

		    		</div>
		    	</div>
			    
			    
		    </Link>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">

				<Nav className="ms-auto">
					<Link className="nav-link" to="/vouchers" exact>Voucher</Link>
					<Link className="nav-link" to="/contracts" exact>Contract</Link>
					<Link className="nav-link" to="/job-orders" exact>Job Order</Link>
					<Link className="nav-link" to="/memos" exact>Memo</Link>
					<Link className="nav-link" to="/letters" exact>Letter</Link>
					<Link className="nav-link" to="/201-files" exact>201 Files</Link>
				</Nav>

		      	<Nav className="ms-auto">
		        
			        
			        <Link className="nav-link" to="/">Home</Link>

			        {(user.id !== null) ?
			        	<Link className="nav-link" to="/logout">Logout</Link>
			        	:
			        	<>
				        	<Link className="nav-link" to="/login">Login</Link>
				        	<Link className="nav-link" to="/register">Register</Link>
			        	</>
			        }

		      	</Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}