import { useState, useEffect } from 'react';

export default function History({match}){
	const token = localStorage.getItem("token")

	const [action, setAction] = useState("")

	const voucherId = match.params.voucherId;

	useEffect(()=> {

	fetch(`${process.env.REACT_APP_API_URL}/vouchers/${voucherId}`, {
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		const history = data.history.map(hist => {

			console.log(data.history.indexOf(hist))

			return (
				<>
				<h5>{hist.action}</h5>
				<h5>{hist.remarks}</h5>
				<h5>{hist.date}</h5>
				<hr/>
				</>
			)
		})

		setAction(history)


		// if(data){
			
		// 	setAction(data.history[0].action)
		// 	setRemarks(data.history[0].remarks)
		// 	setDate(data.history[0].date)
		// }else{
		// 	alert("Something went wrong")
		// }
	})

	}, [])

	return(
		<>
			<h1>{action}</h1>
			
		</>
	)
}