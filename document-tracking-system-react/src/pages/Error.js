import DatePicker from 'react-datepicker';
import { useState } from 'react'
import 'react-datepicker/dist/react-datepicker.css'
const moment = require('moment');

export default function Error(){
	const [ selectedDate, setSelectedDate ] = useState(null)
	const [ selectedDate1, setSelectedDate1 ] = useState(null)
	

	console.log(moment(selectedDate))
	return(
		<>
			<DatePicker 
				selected = {selectedDate} 
				onChange = {date => setSelectedDate(date)}
				dateFormat = 'MMM dd, yyyy'
				maxDate = {selectedDate1}
				isClearable
				showYearDropDown
				scrollableMonthYearDropDown
			/>

			<DatePicker 
				selected = {selectedDate1} 
				onChange = {date => setSelectedDate1(date)}
				dateFormat = 'MMM dd, yyyy'
				minDate = {selectedDate}
				isClearable
				showYearDropDown
				scrollableMonthYearDropDown
			/>
			// Error_
		</>
	)
}