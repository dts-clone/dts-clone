import { Link } from 'react-router-dom';
import voucher from "../icons/voucher.png"
import contract from "../icons/contract.png"
import jobOrder from "../icons/jobOrder.png"
import memo from "../icons/memo.png"
import letter from "../icons/letter.png"
import _201Files from "../icons/_201Files.png"

export default function Home(){

	return(
		<>
			<div className="row m-auto pt-5">
			<Link to="/vouchers" className="col-4 text-center pb-4">
			    <img src={voucher} alt="voucher" className="docImages"></img>
			    <h3>Vouchers</h3>
		    </Link>

		    <Link to="/contracts" className="col-4 text-center pb-4">
			    <img src={contract} alt="contract" className="docImages"></img>
			    <h3>Contracts</h3>
		    </Link>

		    <Link to="/job-orders" className="col-4 text-center pb-4">
			    <img src={jobOrder} alt="job order" className="docImages"></img>
			    <h3>Job Orders</h3>
		    </Link>

		    <Link to="/memos" className="col-4 text-center">
			    <img src={memo} alt="memo" className="docImages"></img>
			    <h3>Memo</h3>
		    </Link>

		    <Link to="/letters" className="col-4 text-center">
			    <img src={letter} alt="letter" className="docImages"></img>
			    <h3>Letters</h3>
		    </Link>

		    <Link to="/201-files" className="col-4 text-center">
			    <img src={_201Files} alt="201 files" className="docImages"></img>
			    <h3>201 Files</h3>
		    </Link>
		    </div>

		</>
	)
}