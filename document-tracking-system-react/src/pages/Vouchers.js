import { useEffect, useState } from 'react';
import { Button, Modal, Form, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css'
const moment = require('moment');

export default function Vouchers(){

	const token = localStorage.getItem("token")

	const [vouchersData, setVouchersData] = useState([]);
	const [voucherId, setVoucherId] = useState("");
	const [drsNo, setDrsNo] = useState("");
	const [payee, setPayee] = useState("");
	const [particulars, setParticulars] = useState("");
	const [amount, setAmount] = useState("");
	const [status, setStatus] = useState("");
	const [sourceOfFunds, setSourceOfFunds] = useState("");
	const [from, setFrom] = useState("");
	const [to, setTo] = useState("");
	const [location, setLocation] = useState("");
	const [remarks, setRemarks] = useState("");
	const [showEdit, setShowEdit] = useState(false)
	const [showArchive, setShowArchive] = useState(false)
	const [showCreate, setShowCreate] = useState(false)

	const fetchAll = () => {
		
		fetch(`${process.env.REACT_APP_API_URL}/vouchers/all`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				drsNo: "",
				payee: "",
				particulars: "",
				amount: "",
				from: "",
				to: "",
				status: "",
				sourceOfFunds: "",
				location: "",
				remarks: ""
			})
		})
		.then(res => res.json())
		.then(data => {
			setVouchersData(data)
		})
	}

	const fetchData = () => {
		if(amount !== ""){
			fetch(`${process.env.REACT_APP_API_URL}/vouchers/all`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					drsNo: drsNo,
					payee: payee,
					particulars: particulars,
					amount: parseInt(amount),
					from: from,
					to: to,
					status: status,
					sourceOfFunds: sourceOfFunds,
					location: location,
					remarks: remarks
				})
			})
			.then(res => res.json())
			.then(data => {
				setVouchersData(data)
			})
		}
		else{
			fetch(`${process.env.REACT_APP_API_URL}/vouchers/all`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					drsNo: drsNo,
					payee: payee,
					particulars: particulars,
					amount: "",
					from: from,
					to: to,
					status: status,
					sourceOfFunds: sourceOfFunds,
					location: location,
					remarks: remarks
				})
			})
			.then(res => res.json())
			.then(data => {
				setVouchersData(data)
			})
		}
	}

	useEffect(() => {
		fetchData()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	const vouchers = vouchersData.map(voucher => {

		const { drsNo, payee, particulars, amount, from, to, status, sourceOfFunds, location, remarks, isArchive } = voucher

		return(
			<tr>
				<td>{drsNo}</td>
				<td>{payee}</td>
				<td>{particulars}</td>
				<td>{amount}</td>
				<td>{moment(from).format("MMM DD, YYYY")}</td>
				<td>{moment(to).format("MMM DD, YYYY")}</td>
				<td>{status}</td>
				<td>{sourceOfFunds}</td>
				<td>
					{location !== ""
					?
					<a rel="noopener noreferrer" target="_blank" href={location}>Link</a>
					:
					<div></div>}
				</td>
				<td>{remarks}</td>

				<td><Link className="btn custom-btn bg-success text-white" to={`/vouchers/${voucher._id}`}>View</Link></td>
				
				{isArchive 
				?
				<>
				<td><Button onClick={() => openEdit(voucher._id)} disabled>Update</Button></td>
				<td><Button onClick={() => openArchive(voucher._id)} disabled>Archive</Button></td>
				</>
				:
				<>
				<td><Button onClick={() => openEdit(voucher._id)} >Update</Button></td>
				<td><Button onClick={() => openArchive(voucher._id)} >Archive</Button></td>
				</>}
			</tr>
		)
	})

	const openArchive = (voucherId) => {

		fetch(`${process.env.REACT_APP_API_URL}/vouchers/${voucherId}`, {
			headers: {Authorization: `Bearer ${token}`}
		})
		.then(res => res.json())
		.then(data => {

			let fromObject = new Date(data.from)
			let toObject = new Date(data.to)

			setVoucherId(data._id)
			setDrsNo(data.drsNo)
			setPayee(data.payee)
			setParticulars(data.particulars)
			setAmount(data.amount)
			setFrom(fromObject)
			setTo(toObject)
			setStatus(data.status)
			setSourceOfFunds(data.sourceOfFunds)
			setLocation("")
			setRemarks(data.remarks)
			
		})

		setShowArchive(true)
	}

	const closeArchive = () => {
		fetchData()
		setVoucherId("")
		setDrsNo("")
		setPayee("")
		setParticulars("")
		setAmount("")
		setFrom("")
		setTo("")
		setStatus("")
		setSourceOfFunds("")
		setLocation("")
		setRemarks("")
		setShowArchive(false)
		
	}


	const archiveVoucher = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/vouchers/archive/${voucherId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				
				location: location,
				isArchive: true
				
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				alert("Voucher successfully archived")
				closeArchive()
			}else{
				alert("Something went wrong")
			}
		})
	}

	const openEdit = (voucherId) => {

			fetch(`${process.env.REACT_APP_API_URL}/vouchers/${voucherId}`, {
				headers: {Authorization: `Bearer ${token}`}
			})
			.then(res => res.json())
			.then(data => {

				let fromObject = new Date(data.from)
				let toObject = new Date(data.to)

				setVoucherId(data._id)
				setDrsNo(data.drsNo)
				setPayee(data.payee)
				setParticulars(data.particulars)
				setAmount(data.amount)
				setFrom(fromObject)
				setTo(toObject)
				setStatus(data.status)
				setSourceOfFunds(data.sourceOfFunds)
				setRemarks(data.remarks)
			})

			setShowEdit(true)
		}

	const closeEdit = () => {
		fetchData()
		setVoucherId("")
		setDrsNo("")
		setPayee("")
		setParticulars("")
		setAmount("")
		setFrom("")
		setTo("")
		setStatus("")
		setSourceOfFunds("")
		setRemarks("")
		setShowEdit(false)
	}


	const updateVoucher = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/vouchers/${voucherId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				drsNo: drsNo,
				payee: payee,
				particulars: particulars,
				amount: amount,
				from: from,
				to: to,
				status: status,
				sourceOfFunds: sourceOfFunds,
				remarks: remarks
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data){
				alert("Voucher successfully updated")
				closeEdit()
			}else{
				alert("Something went wrong")
			}
		})
	}

	const openCreate = () => {
		setVoucherId("")
		setDrsNo("")
		setPayee("")
		setParticulars("")
		setAmount("")
		setFrom("")
		setTo("")
		setStatus("")
		setSourceOfFunds("")
		setLocation("")
		setRemarks("")
		setShowCreate(true)
		fetchAll()
	}

	const closeCreate = () => {
		setVoucherId("")
		setDrsNo("")
		setPayee("")
		setParticulars("")
		setAmount("")
		setFrom("")
		setTo("")
		setStatus("")
		setSourceOfFunds("")
		setLocation("")
		setRemarks("")
		setShowCreate(false)
		fetchAll()
	}

	const createVoucher = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/vouchers/create`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				drsNo: drsNo,
				payee: payee,
				particulars: particulars,
				amount: amount,
				from: from,
				to: to,
				status: status,
				sourceOfFunds: sourceOfFunds,
				location: location,
				remarks: remarks
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Voucher successfully created")
				closeCreate()
				fetchAll()

			}else{
				alert("Something went wrong")
			}
		})
	}

	const searchSpecific = () =>{
	
		fetchData()
	}

	const searchAll = () =>{
		setDrsNo("")
		setPayee("")
		setParticulars("")
		setAmount("")
		setFrom("")
		setTo("")
		setStatus("")
		setSourceOfFunds("")
		setLocation("")
		setRemarks("")
		fetchAll()
	}

	return(
		<>
			<tr>
				<td className="col-2 p-2">
					<div className="p-3" >DRS Number</div>
					<td><Form.Control value={drsNo} onChange={e => setDrsNo(e.target.value)} type="text"/></td>
				</td>

				<td className="col-3 p-2">
					<div className="p-3">Payee</div>
					<td><Form.Control value={payee} onChange={e => setPayee(e.target.value)} type="text"/></td>
				</td>

				<td className="col-1 p-2">
					<div className="p-3">Amount</div>
					<td><Form.Control value={amount} onChange={e => setAmount(e.target.value)} type="number"/></td>
				</td>	
			
				<td className="col-1 p-2">
					<div className="p-3">From</div>
					<DatePicker
						className = "datePicker"
						selected = {from} 
						onChange = {date => setFrom(date)}
						maxDate = {to}
						dateFormat = 'MMM dd, yyyy'
						isClearable
						showYearDropdown
						showMonthDropdown
						scrollableMonthYearDropdown
					/>
				</td>

				<td className="col-1 p-2">
					<div className="p-3">To</div>
					<DatePicker
						className = "datePicker"
						selected = {to} 
						onChange = {date => setTo(date)}
						minDate = {from}
						dateFormat = 'MMM dd, yyyy'
						isClearable
						showYearDropdown
						showMonthDropdown
						scrollableMonthYearDropdown
					/>
				</td>

				<td className="col-1 p-2"><div className="p-3">Particulars</div><td>
					<select className = "datePicker" id="part" value={particulars} onChange={e=>setParticulars(e.target.value)}>
						<option value="">All</option>
						<option>Honorarium</option>
						<option>Salary</option>
						<option>Incentive</option>
						<option>Overtime</option>
					</select>
				</td></td>

				<td className="col-1 p-2"><div className="p-3">Status</div><td>
					<select className = "datePicker" value={status} onChange={e=>setStatus(e.target.value)}>
						<option value="">All</option>
						<option>New</option>
						<option>Completed</option>
						<option>For Approval</option>
						<option>For return</option>
					</select>
				</td></td>

				<td className="col-1 p-2"><div className="p-3">Source of funds</div><td>
					<select className = "datePicker" value={sourceOfFunds} onChange={e=>setSourceOfFunds(e.target.value)}>
						<option value="">All</option>
						<option>9722322-499-439</option>
						<option>9774313-499-439</option>
						<option>9722302-499-439</option>
						<option>9774472-499-439</option>
					</select>
				</td></td>
			</tr>
			<tr className="d-flex">
			<td ><Button variant="success" className="mb-2" onClick={() => openCreate()}>Add Record</Button></td>

			<td className="ms-auto"><td className="p-3"></td><td><Button variant="primary" onClick={() => searchSpecific() }>Search</Button></td></td>
			<td ><td className="p-3"></td><td><Button variant="success" onClick={() => searchAll()}>Reset</Button></td></td>
			</tr>
			
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>DRS No.</th>
						<th>Payee</th>
						<th>Particulars</th>
						<th>Amount</th>
						<th>From</th>
						<th>To</th>
						<th>Status</th>
						<th>Source of funds</th>
						<th>Location</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tbody>
					{vouchers}
				</tbody>
		    </Table>

			<Modal show={showArchive} onHide={closeArchive}>
				<Form onSubmit={e => archiveVoucher(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Archive Voucher Record</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<Form.Group controlId="recordLocation">
							<Form.Label>Location</Form.Label>
							<Form.Control
								value={location}
								onChange={e => setLocation(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeArchive}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => updateVoucher(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Voucher Record</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<div className="row d-flex">
							<Form.Group className="col-6" controlId="recordParticulars">
								<Form.Label className="me-2">Particulars</Form.Label>
								<select className = "datePicker" value={particulars} onChange={e=>setParticulars(e.target.value)}>
									<option>Honorarium</option>
									<option>Salary</option>
									<option>Incentive</option>
									<option>Overtime</option>
								</select>
							</Form.Group>

							<Form.Group className="col-6" controlId="recordStatus">
								<Form.Label className="me-2">Status</Form.Label>
								<select className = "datePicker" value={status} onChange={e=>setStatus(e.target.value)}>
									<option>New</option>
									<option>Completed</option>
									<option>For Approval</option>
									<option>For return</option>
								</select>
							</Form.Group>

							<Form.Group className="col-6" controlId="recordSourceOfFunds">
								<Form.Label className="me-2">Source of funds</Form.Label>
								<select className = "datePicker" value={sourceOfFunds} onChange={e=>setSourceOfFunds(e.target.value)}>
									<option>9722322-499-439</option>
									<option>9774313-499-439</option>
									<option>9722302-499-439</option>
									<option>9774472-499-439</option>
								</select>
							</Form.Group>
						</div>

						<Form.Group controlId="recordDrsNo">
							<Form.Label>DRS Number</Form.Label>
							<Form.Control
								value={drsNo}
								onChange={e => setDrsNo(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="recordFullName">
							<Form.Label>Payee</Form.Label>
							<Form.Control
								value={payee}
								onChange={e => setPayee(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="recordAmount">
							<Form.Label>Amount</Form.Label>
							<Form.Control
								value={amount}
								onChange={e => setAmount(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>

						<div className="row d-flex">
							<Form.Group className="col-6">
								<Form.Label>From</Form.Label>
								<DatePicker
									className = "datePicker"
									selected = {from} 
									onChange = {date => setFrom(date)}
									maxDate = {to}
									dateFormat = 'MMM dd, yyyy'
									isClearable
									showYearDropdown
									showMonthDropdown
									scrollableMonthYearDropdown
									required
								/>
							</Form.Group>

							<Form.Group className="col-6">
								<Form.Label>To</Form.Label>
								<DatePicker
									className = "datePicker"
									selected = {to} 
									onChange = {date => setTo(date)}
									minDate = {from}
									dateFormat = 'MMM dd, yyyy'
									isClearable
									showYearDropdown
									showMonthDropdown
									scrollableMonthYearDropdown
									required
								/>
							</Form.Group>
						</div>

						<Form.Group controlId="recordFileName">
							<Form.Label>Remarks</Form.Label>
							<Form.Control
								value={remarks}
								onChange={e => setRemarks(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		    <Modal show={showCreate} onHide={closeCreate}>
				<Form onSubmit={e => createVoucher(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Create Voucher Record</Modal.Title>
					</Modal.Header>

					<Modal.Body>

						<div className="row d-flex">
							<Form.Group className="col-6" controlId="recordParticulars">
								<Form.Label className="me-2">Particulars</Form.Label>
								<select className = "datePicker" value={particulars} onChange={e=>setParticulars(e.target.value)}>
									<option value="">---</option>
									<option>Honorarium</option>
									<option>Salary</option>
									<option>Incentive</option>
									<option>Overtime</option>
								</select>
							</Form.Group>
							

							<Form.Group className="col-6" controlId="recordStatus">
								<Form.Label className="me-2">Status</Form.Label>
								<select className = "datePicker" value={status} onChange={e=>setStatus(e.target.value)}>
									<option value="">---</option>
									<option>New</option>
									<option>Completed</option>
									<option>For Approval</option>
									<option>For return</option>
								</select>
							</Form.Group>

							<Form.Group className="col-6" controlId="recordSourceOfFunds">
								<Form.Label className="me-2">Source of funds</Form.Label>
								<select className = "datePicker" value={sourceOfFunds} onChange={e=>setSourceOfFunds(e.target.value)}>
									<option value="">---</option>
									<option>9722322-499-439</option>
									<option>9774313-499-439</option>
									<option>9722302-499-439</option>
									<option>9774472-499-439</option>
								</select>
							</Form.Group>
						</div>

						<Form.Group controlId="recordDrsNo">
							<Form.Label>DRS Number</Form.Label>
							<Form.Control
								value={drsNo}
								onChange={e => setDrsNo(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
	
						<Form.Group controlId="recordFullName">
							<Form.Label>Payee</Form.Label>
							<Form.Control
								value={payee}
								onChange={e => setPayee(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="recordAmount">
							<Form.Label>Amount</Form.Label>
							<Form.Control
								value={amount}
								onChange={e => setAmount(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>

						<div className="row d-flex">
							<Form.Group className="col-6">
								<Form.Label>From</Form.Label>
								<DatePicker
									className = "datePicker"
									selected = {from} 
									onChange = {date => setFrom(date)}
									maxDate = {to}
									dateFormat = 'MMM dd, yyyy'
									isClearable
									showYearDropdown
									showMonthDropdown
									scrollableMonthYearDropdown
									required
								/>
							</Form.Group>

							<Form.Group className="col-6">
								<Form.Label>To</Form.Label>
								<DatePicker
									className = "datePicker"
									selected = {to} 
									onChange = {date => setTo(date)}
									minDate = {from}
									dateFormat = 'MMM dd, yyyy'
									isClearable
									showYearDropdown
									showMonthDropdown
									scrollableMonthYearDropdown
									required
								/>
							</Form.Group>
						</div>


						{/*<Form.Group controlId="recordLocation">
							<Form.Label>Location</Form.Label>
							<Form.Control
								value={location}
								onChange={e => setLocation(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>*/}

						<Form.Group controlId="recordFileName">
							<Form.Label>Remarks</Form.Label>
							<Form.Control
								value={remarks}
								onChange={e => setRemarks(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeCreate}>Close</Button>
						{(particulars === "" || status === "" || sourceOfFunds === "")
							? <Button variant="success" type="submit" disabled>Submit</Button>
							: <Button variant="success" type="submit">Submit</Button>
						}
					</Modal.Footer>
				</Form>
			</Modal>

		</>
	)
}
